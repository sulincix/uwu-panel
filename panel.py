import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Wnck', '3.0')
from gi.repository import Gtk, Wnck, Gdk
import Xlib
from Xlib.display import Display
from Xlib import X

class Panel:
    def __init__(self):
        """Create new empty panel"""
        self.window = Gtk.Window()
        self.window.set_name("bar")
        self.window.set_type_hint(Gdk.WindowTypeHint.DOCK)
        self.window.set_keep_above(False)
        self.window.set_decorated(False)
        self.window.set_position(Gtk.WindowPosition.NONE)
        self.window.connect("delete-event", Gtk.main_quit)
        self.window.set_skip_taskbar_hint(True)
        self.window.set_skip_pager_hint(True)
        self.window.set_size_request(Gdk.Screen.get_width(Gdk.Screen.get_default()),0)
        self.window.move(0,0)
        self.applet_box=Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        self.window.add(self.applet_box)        
    def init(self):
        """We must call this fuction before Gtk.main"""
        self.window.show_all()
        self.height=self.window.get_allocated_height()
        display = Display()
        topw = display.create_resource_object('window',
                                          self.window.get_toplevel().get_window().get_xid())
        # http://python-xlib.sourceforge.net/doc/html/python-xlib_21.html#SEC20
        topw.change_property(display.intern_atom('_NET_WM_STRUT'),
                           display.intern_atom('CARDINAL'), 32,
                           [0,0, self.height,0 ],
                           X.PropModeReplace)
        topw.change_property(display.intern_atom('_NET_WM_STRUT_PARTIAL'),
                           display.intern_atom('CARDINAL'), 32,
                           [0, 0, self.height,0, 0, 0, 0, 0, 0, 0, 0, 0,],
                           X.PropModeReplace)
        self=self.window

    def add(self,widget):
        self.applet_box.pack_start(widget,0,0,False)
