import os
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk

class Launcher:
    def __init__(self,panel=None):
        """If you add panel arg, launcher automatically add in panel"""
        self.panel=panel
    
    def create_launcher(self,cmd="",label=""):
        """Create Application launcher"""
        self.button=Gtk.Button()
        self.command=cmd
        self.button.connect("clicked",self.run_cmd)
        self.button.set_label(label)
        if self.panel:
            self.panel.add(self.button)
        return self.button
    def init(self):
        self.panel.window.add(self.button)
       
    def run_cmd(self,widget):
        os.system(self.command+" &")
