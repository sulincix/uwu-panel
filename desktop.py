import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk
class Desktop:
    def __init__(self,image=""):
        self.window = Gtk.Window()
        self.window.set_name("desktop")
        self.window.set_type_hint(Gdk.WindowTypeHint.DESKTOP)
        self.window.set_keep_below(False)
        self.window.set_decorated(False)
        self.window.set_size_request(Gdk.Screen.get_width(Gdk.Screen.get_default()),Gdk.Screen.get_height(Gdk.Screen.get_default()))
        # TODO scale image fit size
        #self.window.add(Gtk.Image.new_from_file(image))
        self.window.show_all()
